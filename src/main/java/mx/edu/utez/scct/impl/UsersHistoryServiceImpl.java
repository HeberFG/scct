package mx.edu.utez.scct.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.scct.entity.UsersHistory;
import mx.edu.utez.scct.repository.UsersHistoryRepository;
import mx.edu.utez.scct.service.UsersHistoryService;

@Service
public class UsersHistoryServiceImpl implements UsersHistoryService{
	
	@Autowired
    UsersHistoryRepository userRepository;

	@Override
	public List<UsersHistory> listAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public UsersHistory findUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}
