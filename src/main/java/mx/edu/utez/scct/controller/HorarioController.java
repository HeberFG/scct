package mx.edu.utez.scct.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Horario;
import mx.edu.utez.scct.entity.Rol;
import mx.edu.utez.scct.entity.User;
import mx.edu.utez.scct.impl.HorarioServiceImpl;
import mx.edu.utez.scct.impl.UserServiceImpl;
import mx.edu.utez.scct.impl.VentanillaServiceImpl;

@Controller
@RequestMapping(value = "/horarios")
public class HorarioController {

    @Autowired
    HorarioServiceImpl horarioServiceImpl;

    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    VentanillaServiceImpl ventanillaServiceImpl;

    @GetMapping("/listar")
    @Secured("ROLE_ADMIN")
    public String listar(Model modelo) {
        modelo.addAttribute("listaHorarios", horarioServiceImpl.listarHorarios());
        modelo.addAttribute("bandera", false);
        return "horarios/listaHorarios";
    }

    @GetMapping("/listar/personal")
    @Secured("ROLE_EMPLEADO")
    public String listarUser(Model modelo, Authentication authentication) {
        User user = userServiceImpl.findUserByUsername(authentication.getName());
        modelo.addAttribute("listaHorarios", horarioServiceImpl.findByUser(user));
        modelo.addAttribute("bandera", true);
        return "horarios/listaHorariosEmpleado";
    }

    @GetMapping("/crear")
    @Secured("ROLE_EMPLEADO")
    public String crear(Horario horario, Model modelo, Authentication authentication) {
        modelo.addAttribute("listaVentanilla", ventanillaServiceImpl.listarVentanillas());
        modelo.addAttribute("username", authentication.getName());
        LocalDate now = LocalDate.now();
        modelo.addAttribute("now", now);
        return "horarios/formHorarios";
    }

    @PostMapping("/editar")
    @Secured("ROLE_EMPLEADO")
    public String editar(@RequestParam("idhorarios") Long idhorarios, Model modelo, RedirectAttributes redirectAttributes) {
        Horario horario = horarioServiceImpl.findById(idhorarios);
        if (horario != null) {
            modelo.addAttribute("horario", horario);
            SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatterHour = new SimpleDateFormat("HH:mm");
            modelo.addAttribute("fechaDiaUpdate", formatterDate.format(horario.getDia()));
            modelo.addAttribute("horaInicioUpdate", formatterHour.format(horario.getHora_inicio()));
            modelo.addAttribute("horaFinUpdate", formatterHour.format(horario.getHora_fin()));
            modelo.addAttribute("listaVentanilla", ventanillaServiceImpl.listarVentanillas());
            return "horarios/editHorarios";
        }
        redirectAttributes.addFlashAttribute("msg_errorEditar", "Registro No Encontrado");
        return "redirect:/horarios/listar";
    }

    @PostMapping("/guardar")
    @Secured("ROLE_EMPLEADO")
    public String guardar(
            @RequestParam("fechaDia") String fechaDia,
            @RequestParam("horaInicio") String horaInicio,
            @RequestParam("horaFinal") String horaFinal, Horario horario, Model modelo,
            RedirectAttributes redirectAttributes, BindingResult result) throws ParseException {
        boolean isRegistro = true;
        if (horario.getIdhorarios() != null) {
            isRegistro = false;
        }
        int repeticiones = 1;
        if (horario.getRepeticiones() != null) {
            repeticiones = horario.getRepeticiones();
        }
        boolean respuesta = false;
        String successMessage = "";
        String errorMessage = "";
        String fechaTempo = fechaDia;
        int registrosExitosos = 0;
        for (int x = 0; x < repeticiones; x++) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String horaInicioRegistro = fechaTempo + " " + horaInicio + ":00";
            String horaFinalRegistro = fechaTempo + " " + horaFinal + ":00";
            Date fechaDiaRegistro = formatter.parse(fechaTempo + " 00:00");
            Date inicioH = formatter.parse(horaInicioRegistro);
            Date finH = formatter.parse(horaFinalRegistro);
            boolean bandera = true;
            if (isRegistro) { // Create
                List<Horario> listaDia = horarioServiceImpl.listarPorDiaAndVentanilla(fechaDiaRegistro,
                        horario.getVentanilla().getIdventanilla());
                for (int i = 0; i < listaDia.size(); i++) {
                    if (inicioH.getTime() >= listaDia.get(i).getHora_inicio().getTime()
                            || finH.getTime() <= listaDia.get(i).getHora_fin().getTime()) {
                        errorMessage = "La ventanilla " + listaDia.get(i).getVentanilla().getNumero()
                                + " no puede registrar entre las horas de otro horario";
                        bandera = false;
                    }
                }
                if (bandera) {
                    horario.setDia(fechaDiaRegistro);
                    horario.setHora_inicio(inicioH);
                    horario.setHora_fin(finH);
                    successMessage = "Registro Creado Exitosamente";
                }
            } else { // Update
                List<Horario> listaUpdate = horarioServiceImpl.listarPorDiaAndVentanillaAndHorario(fechaDiaRegistro,
                        horario.getVentanilla().getIdventanilla(), horario.getIdhorarios());
                for (int i = 0; i < listaUpdate.size(); i++) {
                    if (inicioH.getTime() >= listaUpdate.get(i).getHora_inicio().getTime()
                            || finH.getTime() <= listaUpdate.get(i).getHora_fin().getTime()) {
                        errorMessage = "No se puede poner fechas encimadas";
                        bandera = false;
                    }
                }
                if (bandera) {
                    horario.setDia(fechaDiaRegistro);
                    horario.setHora_inicio(inicioH);
                    horario.setHora_fin(finH);
                    horario.setRepeticiones(horario.getRepeticiones());
                    horario.setUser(horario.getUser());
                    successMessage = "Registro Modificado Exitosamente";
                }
            }
            if (bandera) {
                Horario horarioTempo = new Horario();
                horarioTempo.setDia(horario.getDia());
                horarioTempo.setHora_inicio(horario.getHora_inicio());
                horarioTempo.setHora_fin(horario.getHora_fin());
                horarioTempo.setRepeticiones(1);
                horarioTempo.setUser(horario.getUser());
                horarioTempo.setVentanilla(horario.getVentanilla());
                if (!isRegistro) {
                    horarioTempo.setIdhorarios(horario.getIdhorarios());
                }
                respuesta = horarioServiceImpl.guardarHorario(horarioTempo);
                if (!respuesta) {
                    errorMessage = "Registro Fallido por favor intente de nuevo";
                } else {
                    registrosExitosos = registrosExitosos + 1;
                }
                if (!isRegistro) {
                    break;
                }
            }
            SimpleDateFormat formatterTempo = new SimpleDateFormat("yyyy-MM-dd");
            Date fechaCambio = formatter.parse(fechaTempo + " 00:00");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaCambio);
            calendar.add(Calendar.DAY_OF_YEAR, 7);
            Date dateOneWeek = calendar.getTime();
            fechaTempo = formatterTempo.format(dateOneWeek);
        }
        if (registrosExitosos > 0) {
            if (isRegistro) {
                redirectAttributes.addFlashAttribute("msg_success",
                        successMessage + ", " + registrosExitosos + " Registros Exitosos");
            }else{  
                redirectAttributes.addFlashAttribute("msg_success",
                        successMessage);
            }
            return "redirect:/horarios/listar/personal";
        } else {
            redirectAttributes.addFlashAttribute("msg_errorRegistro", errorMessage);
            if (isRegistro) {
                return "redirect:/horarios/crear";
            } else {
                return "redirect:/horarios/editar/" + horario.getIdhorarios();
            }
        }
    }

    @GetMapping("/eliminar/{idhorarios}")
    @Secured({ "ROLE_EMPLEADO", "ROLE_ADMIN" })
    public String eliminar(@PathVariable long idhorarios, RedirectAttributes redirectAttributes, Authentication authentication) {

        String retornoHorarioExitoso = "";
        String retornoHorarioFallido = "";
        User usuariologueadoHorario = userServiceImpl.findUserByUsername(authentication.getName());
        for (Rol rol : usuariologueadoHorario.getRols()) {
            if (rol.getName().equals("ROLE_ADMIN")) {
                retornoHorarioExitoso = "redirect:/horarios/listar";
                retornoHorarioFallido = "redirect:/horarios/listar";
            } else {
                retornoHorarioExitoso = "redirect:/horarios/listar/personal";
                retornoHorarioFallido = "redirect:/horarios/listar/personal";
            }
        }

        boolean respuesta = horarioServiceImpl.eliminar(idhorarios);
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", "Registro Eliminado");
            return retornoHorarioExitoso;
        } else {
            redirectAttributes.addFlashAttribute("msg_errorEliminar", "Eliminacion Fallida");
            
        }
        return retornoHorarioFallido;
    }
}
