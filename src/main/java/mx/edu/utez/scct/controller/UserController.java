package mx.edu.utez.scct.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Cita;
import mx.edu.utez.scct.entity.Rol;
import mx.edu.utez.scct.entity.Solicitante;
import mx.edu.utez.scct.entity.User;
import mx.edu.utez.scct.impl.CarreraServiceImpl;
import mx.edu.utez.scct.impl.CitaServiceImpl;
import mx.edu.utez.scct.impl.RolServiceImpl;
import mx.edu.utez.scct.impl.SolicitanteServiceImpl;
import mx.edu.utez.scct.impl.UserServiceImpl;
import mx.edu.utez.scct.impl.UsersHistoryServiceImpl;

@Controller
@RequestMapping(value = "/usuarios")
public class UserController {

    @Autowired
    CarreraServiceImpl carreraServiceImpl;

    @Autowired
    CitaServiceImpl citaServiceImpl;

    @Autowired
    RolServiceImpl rolServiceImpl;

    @Autowired
    SolicitanteServiceImpl solicitanteServiceImpl;

    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    UsersHistoryServiceImpl usersHistoryServiceImpl;

    @GetMapping("/lista")
    @Secured("ROLE_ADMIN")
    public String listar(Model model) {
        model.addAttribute("userList", userServiceImpl.listAllUsers());
        return "administrador/userList";
    }

    @GetMapping("/listaBitacora")
    @Secured("ROLE_ADMIN")
    public String listarBitacora(Model model) {
        model.addAttribute("userListBitacora", usersHistoryServiceImpl.listAllUsers());
        return "administrador/userListBitacora";
    }

    @GetMapping("/registroUsuario")
    @Secured("ROLE_ADMIN")
    public String crearUser(User user) {
        return "administrador/userCreate";
    }

    @PostMapping("/guardarUsuario")
    @Secured("ROLE_ADMIN")
    public String saveUser(@Valid @ModelAttribute("user") User user, BindingResult result, RedirectAttributes redirectAttributes) {
        User userInsert = new User();
        userInsert = userServiceImpl.saveUser(user);
        if (result.hasErrors()) {
            return "administrador/userCreate";
        }
        if(userInsert != null){
            redirectAttributes.addFlashAttribute("msg_success", "Usuario Creado Exitosamente");
            return "redirect:/usuarios/lista";
            
        }
        redirectAttributes.addFlashAttribute("msg_errorUser", "Registro Fallido por favor intente de nuevo");
        return "administrador/userCreate";
    }


    @PostMapping("/modificarUsuario")
    @Secured("ROLE_ADMIN")
    public String updateUser(@Valid @ModelAttribute("user") User user, BindingResult result, RedirectAttributes redirectAttributes) {  
        user.setUsername(user.getUsername());
        user.setNombre(user.getNombre());
        user.setApellido1(user.getApellido1());
        user.setApellido2(user.getApellido2());
        user.setEnabled(user.getEnabled());
        user.setPassword(user.getPassword());
        user.setRols(user.getRols());
        User usuarioUpdate = userServiceImpl.updateUser(user);
        if (result.hasErrors()) {
            return "administrador/userCreate";
        }
        if(usuarioUpdate != null){
            redirectAttributes.addFlashAttribute("msg_success", "Usuario Modificado Exitosamente");
            return "redirect:/usuarios/lista";
            
        }
        redirectAttributes.addFlashAttribute("msg_errorUserUpdate", "Registro Fallido por favor intente de nuevo");
        return "administrador/userCreate";
    }

    @GetMapping("/registroSolicitante")
    public String crearSolicitante(User user, Solicitante solicitante, Model model) {
        model.addAttribute("carreraList", carreraServiceImpl.listAllCarreras());
        return "registro";
    }

    @PostMapping("/guardarSolicitante")
    public String saveSolicitante(@Valid @ModelAttribute("user") User user,
        Model modelo ,  @Valid @ModelAttribute("solicitante") Solicitante solicitante, BindingResult result,  RedirectAttributes redirectAttributes) {
            user.setUsername(user.getUsername());
            user.setNombre(user.getNombre());
            user.setApellido1(user.getApellido1());
            user.setApellido2(user.getApellido2());
            user.setEnabled(user.getEnabled());
            user.setPassword(user.getPassword());
            user.setRols(user.getRols());
        
            if (result.hasErrors()) {
                modelo.addAttribute("carreraList", carreraServiceImpl.listAllCarreras());
                return "registro";
            }
            User usuario = userServiceImpl.saveUser(user);
            solicitante.setUser(usuario);
            
           boolean saveSolicitante =  solicitanteServiceImpl.saveSolicitante(solicitante);
            if(saveSolicitante){
                redirectAttributes.addFlashAttribute("msg_success", "Te registraste de manera exitosa");
                return "redirect:/login";
            
            }
            redirectAttributes.addFlashAttribute("msg_errorSolicitanteUpdate", "Registro Fallido por favor intente de nuevo");
            return "registro";
    }

    @PostMapping("/modificarSolicitante")
    public String updateSolicitante(@Valid @ModelAttribute("user") User user,
        Model modelo ,  @Valid @ModelAttribute("solicitante") Solicitante solicitante, BindingResult result, RedirectAttributes redirectAttributes) {
            user.setUsername(user.getUsername());
            user.setNombre(user.getNombre());
            user.setApellido1(user.getApellido1());
            user.setApellido2(user.getApellido2());
            user.setEnabled(user.getEnabled());
            user.setPassword(user.getPassword());
            user.setRols(user.getRols());
            
            if (result.hasErrors()) {
                modelo.addAttribute("carreraList", carreraServiceImpl.listAllCarreras());
                return "administrador/editSolicitante";
            }
            User usuario = userServiceImpl.updateUser(user);
            solicitante.setUser(usuario);
            
            boolean respuestaUpdate = solicitanteServiceImpl.updateSolicitante(solicitante);

            if(respuestaUpdate){
                redirectAttributes.addFlashAttribute("msg_success", "Solicitante Modificado Exitosamente");
                return "redirect:/usuarios/lista";
                
            }
            redirectAttributes.addFlashAttribute("msg_errorSolicitanteUpdate", "Registro Fallido por favor intente de nuevo");
            return "administrador/editSolicitante";
    }

    @PostMapping("/detalleUsuario")
    @Secured("ROLE_ADMIN")
    public String detalle(@RequestParam("username") String username, Model model) {
        User usuario = userServiceImpl.findUserByUsername(username);
        model.addAttribute("user", usuario);
        for (Rol rol : usuario.getRols()) {
            if (rol.getName().equals("ROLE_USER")) {
                model.addAttribute("detalle", solicitanteServiceImpl.findByUser(usuario));
                return "administrador/solicitanteDetail";
            }
        }
        return "administrador/userDetail";
    }



    @GetMapping("/eliminarUsuario/{username}")
    @Secured("ROLE_ADMIN")
    public String eliminar(@PathVariable("username") String username,  RedirectAttributes redirectAttributes,  Authentication authentication) {
        String errorMessageUser = "";
        User usuario = userServiceImpl.findUserByUsername(username);
        String retorno = "redirect:/usuarios/lista";
        for (Rol rol : usuario.getRols()) {
            if (rol.getName().equals("ROLE_USER")) {
                Solicitante solicitante = solicitanteServiceImpl.findByUser(usuario);
                List<Cita> deleteCitaSolicitante = citaServiceImpl.deleteByUser(solicitante.getUser());
                boolean deleteSolicitante = solicitanteServiceImpl.deleteSolicitante(solicitante.getMatricula());
               
                
                if(!deleteCitaSolicitante.isEmpty() && !deleteSolicitante){
                    errorMessageUser = "Ocurrio un error intentalo de nuevo";
                }
                
                
            }
        }
        boolean respuestaDelete = false;
        User usuarioLogueado = userServiceImpl.findUserByUsername(authentication.getName());
        
        if(usuarioLogueado.getUsername() == usuario.getUsername()){
            errorMessageUser = "No puedes eliminar este usuario por que es el que estas usando";
             
        }else{
            respuestaDelete = userServiceImpl.deleteUser(username);
            errorMessageUser = "Ocurrio un error por favor intente mas tarde";
        }

        if(respuestaDelete){
            redirectAttributes.addFlashAttribute("msg_success", "Usuario Eliminado Exitosamente");
            
        }else{
            
            redirectAttributes.addFlashAttribute("msg_errorUser", errorMessageUser);
        }
        return retorno;

        
    }

    @PostMapping("/editarUsuario")
    @Secured("ROLE_ADMIN")
    public String editar(@RequestParam("username") String username, Model model, User user) {
        User usuario = userServiceImpl.findUserByUsername(username);
        model.addAttribute("user", usuario);
        for (Rol rol : usuario.getRols()) {
            if (rol.getName().equals("ROLE_USER")) {
                Solicitante solicitanteSend = solicitanteServiceImpl.findByUser(usuario);
                model.addAttribute("solicitante", solicitanteSend);
                model.addAttribute("carreraList", carreraServiceImpl.listAllCarreras());
                return "administrador/editSolicitante";
            }
        }
        return "administrador/editUser";
    }
}
