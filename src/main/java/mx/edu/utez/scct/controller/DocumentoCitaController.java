package mx.edu.utez.scct.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Cita;
import mx.edu.utez.scct.entity.Documento;
import mx.edu.utez.scct.entity.DocumentoCita;
import mx.edu.utez.scct.impl.CitaServiceImpl;
import mx.edu.utez.scct.impl.DocumentoCitaServiceImpl;
import mx.edu.utez.scct.impl.DocumentoServiceImpl;
import mx.edu.utez.scct.util.ImagenUtileria;

@Controller
@RequestMapping(value = "/documentosCitas")
public class DocumentoCitaController {

    @Autowired
    private CitaServiceImpl citaService;

    @Autowired
    private DocumentoCitaServiceImpl documentoCitaService;

    @Autowired
    private DocumentoServiceImpl documentoService;

    @GetMapping("/documentacion/{idCita}/{idDocumento}")
    @Secured("ROLE_USER")
    public String crearDocumento(@PathVariable long idCita, @PathVariable long idDocumento, Model modelo,
            RedirectAttributes redirectAttributes) {
        modelo.addAttribute("idCita", idCita);
        modelo.addAttribute("idDocumento", idDocumento);
        return "citas/formDocumentacion";
    }

    @PostMapping("/guardar")
    @Secured("ROLE_USER")
    public String guardarDocumento(@RequestParam("cita") Long cita,@RequestParam("documento") Long documento, @RequestParam("archivo") MultipartFile multipartFile,
        Model modelo, RedirectAttributes redirectAttributes)  {
        Cita citaObtener = citaService.mostrar(cita);
        Documento documentoObtener = documentoService.mostrar(documento);

        


        DocumentoCita documentoCita = new DocumentoCita();
        documentoCita.setCita(citaObtener);
        documentoCita.setDocumento(documentoObtener);

        if(!multipartFile.isEmpty()) {

			// Establecer directorio local para subida de archivos; en prod: /var/www/html
			String ruta = "C:/Documentos/Archivos";
			String nombreImagen = ImagenUtileria.guardarDocumento(multipartFile, ruta);

			if(nombreImagen != null) {
				documentoCita.setArchivo(nombreImagen);
			}

		}

        boolean respuesta = documentoCitaService.guardar(documentoCita);
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", "Documento Registrado Exitosamente");
            return "redirect:/citas/mostrar/"+cita;
        }
        redirectAttributes.addFlashAttribute("msg_error", "Ocurrio un error Intente de nuevo");
        return "redirect:/documentosCitas/documentacion/" + cita;
    }
}
