package mx.edu.utez.scct.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Transaccion;
import mx.edu.utez.scct.impl.CitaServiceImpl;
import mx.edu.utez.scct.impl.TransaccionServiceImpl;

@Controller
@RequestMapping(value = "/transacciones")
public class TransaccionController {

    @Autowired
    private TransaccionServiceImpl transaccionServiceImpl;

    @Autowired
    private CitaServiceImpl citaServiceImpl;

    @GetMapping("/listar")
    @Secured("ROLE_ADMIN")
    public String listarServicios(Model model) {
        model.addAttribute("listaTransacciones", transaccionServiceImpl.listar());
        return "transacciones/listarTransacciones";
    }

    @GetMapping("/crear/{idCita}")
    @Secured("ROLE_USER")
    public String crear(@PathVariable("idCita") Long idCita,Transaccion transaccion, Model modelo) {
        Double monto = citaServiceImpl.mostrar(idCita).getServicio().getPrecio();
        modelo.addAttribute("idCita", idCita);
        modelo.addAttribute("precio", monto);
        return "transacciones/formTransaccion";
    }

    @GetMapping("/crearPago")
    @Secured("ROLE_USER")
    public String crearPago(Transaccion transaccion, Model model) {
        
        
        return "transacciones/wizardPago";
    }

    @PostMapping( "/guardar")
    @Secured("ROLE_USER")
    public String guardar(@Valid @ModelAttribute("transaccion") Transaccion transaccion,
            BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        boolean respuesta = transaccionServiceImpl.guardar(transaccion);
        if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                System.out.println("Error : " + error.getDefaultMessage());
            }
            return "transacciones/formTransaccion";
        }
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", "¡Transacción exitoso!");
            return "redirect:/citas/mostrar/"+ transaccion.getCita().getIdCita();
        } else {
            redirectAttributes.addFlashAttribute("msg_error", "¡Transacción fallido!");
            return "redirect:/transacciones/crear"+ transaccion.getCita().getIdCita();
        }
    }

    @GetMapping("/eliminar/{id}")
    @Secured("ROLE_ADMIN")
    public String eliminar(@PathVariable long id, RedirectAttributes redirectAttributes) {
        boolean respuesta = transaccionServiceImpl.eliminar(id);
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", "¡Eliminacion exitosa!");
            return "redirect:/transacciones/listar";
        } else {
            redirectAttributes.addFlashAttribute("msg_errorEliminar", "¡Eliminación fallida!");
            return "redirect:/transacciones/listar";
        }
    }

    @GetMapping("/mostrar/{id}")
    @Secured({ "ROLE_USER", "ROLE_ADMIN" })
    public String mostrar(@PathVariable long id, Model modelo, RedirectAttributes redirectAttributes) {
        Transaccion transaccion = transaccionServiceImpl.mostrar(id);
        if (transaccion != null) {
            modelo.addAttribute("transaccion", transaccion);
            return "transacciones/mostrarTransaccion";
        }
        redirectAttributes.addFlashAttribute("msg_errorMostrar", "consulta fallida");
        return "redirect:/transaccion/listar";
    }

    @GetMapping("/editar/{id}")
    @Secured("ROLE_ADMIN")
    public String editar(@PathVariable long id, Model modelo, RedirectAttributes redirectAttributes) {
        Transaccion transaccion = transaccionServiceImpl.mostrar(id);
        if (transaccion != null) {
            modelo.addAttribute("transaccion", transaccion);
            return "transacciones/formTransaccion";
        }
        redirectAttributes.addFlashAttribute("msg_error", "Registro no encontrado");
        return "redirect:/transacciones/listar";
    }
}