package mx.edu.utez.scct.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Documento;
import mx.edu.utez.scct.impl.DocumentoServiceImpl;

@Controller
@RequestMapping(value = "/documentos")
public class DocumentoController {

    @Autowired
    DocumentoServiceImpl documentoServiceImpl;

 

    @GetMapping("/listar")
    @Secured("ROLE_ADMIN")
    public String listarServicios(Model model) {
        model.addAttribute("listaDocumentos", documentoServiceImpl.listar());
        return "documentos/listarDocumentos";
    }

    @GetMapping("/crear")
    @Secured("ROLE_ADMIN")
    public String crearDocumento(Documento documento, Model model) {
        return "documentos/formDocumento";
    }

    @PostMapping("/guardar")
    @Secured("ROLE_ADMIN")
    public String guardar(@Valid @ModelAttribute("documento") Documento documento, BindingResult result,
            Model model, RedirectAttributes redirectAttributes) {
        boolean respuesta = false;
        String successMessage;
        if(documento.getIdDocumento() == null){
            respuesta = documentoServiceImpl.guardar(documento);
            successMessage = "Documento Registrado Exitosamente";
        }else{
            respuesta = documentoServiceImpl.guardar(documento);
            successMessage = "Documento Modificado Exitosamente";
        }
        if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                System.out.println("Error : " + error.getDefaultMessage());
            }
            return "documentos/formDocumento";
        }
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", successMessage);
            return "redirect:/documentos/listar";
        } else {
            redirectAttributes.addFlashAttribute("msg_error", "¡Registro de Documento fallido!");
            return "redirect:/documentos/crear";
        }
    }

    @GetMapping("/eliminar/{id}")
    @Secured("ROLE_ADMIN")
    public String eliminarServicio(@PathVariable long id, RedirectAttributes redirectAttributes) {
        boolean respuesta = documentoServiceImpl.eliminar(id);
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", "¡Eliminacion exitosa!");
            return "redirect:/documentos/listar";
        } else {
            redirectAttributes.addFlashAttribute("msg_error", "¡Eliminación fallida!");
            return "redirect:/documentos/listar";
        }
    }

    @PostMapping("/mostrar")
    @Secured("ROLE_ADMIN")
    public String mostrar(@RequestParam("idDocumento") long id, Model modelo, RedirectAttributes redirectAttributes) {
        Documento documento = documentoServiceImpl.mostrar(id);
        if (documento != null) {
            modelo.addAttribute("documento", documento);
            return "documentos/mostrarDocumento";
        }
        redirectAttributes.addFlashAttribute("msg_error", "consulta fallida");
        return "redirect:/documentos/listar";
    }

    @PostMapping("/editar")
    @Secured("ROLE_ADMIN")
    public String editar(@RequestParam("idDocumento") long id, Model modelo, RedirectAttributes redirectAttributes) {
        Documento documento = documentoServiceImpl.mostrar(id);
        if (documento != null) {
            modelo.addAttribute("documento", documento);
            return "documentos/editDocumento";
        }
        redirectAttributes.addFlashAttribute("msg_error", "Registro no encontrado");
        return "redirect:/documentos/listar";
    }
}