package mx.edu.utez.scct.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Cita;
import mx.edu.utez.scct.entity.DocumentoCita;
import mx.edu.utez.scct.entity.Rol;
import mx.edu.utez.scct.entity.Transaccion;
import mx.edu.utez.scct.entity.User;
import mx.edu.utez.scct.impl.CitaServiceImpl;
import mx.edu.utez.scct.impl.DocumentoCitaServiceImpl;
import mx.edu.utez.scct.impl.ServicioServiceImpl;
import mx.edu.utez.scct.impl.TransaccionServiceImpl;
import mx.edu.utez.scct.impl.UserServiceImpl;
import mx.edu.utez.scct.impl.VentanillaServiceImpl;

@Controller
@RequestMapping(value = "/citas")
public class CitaController {

    @Autowired
    CitaServiceImpl citaService;

    @Autowired
    TransaccionServiceImpl transaccionServiceImpl;

    @Autowired
    VentanillaServiceImpl ventanillaService;

    @Autowired
    ServicioServiceImpl servicioService;

    @Autowired
    DocumentoCitaServiceImpl documentoCitaService;

    @Autowired
    UserServiceImpl userServiceImpl;

    @GetMapping("/crear")
    @Secured("ROLE_USER")
    public String crear(Cita cita, Model modelo, Authentication authentication) {
        LocalDate now = LocalDate.now();
        LocalDateTime fecha = LocalDateTime.now();
        int hora = fecha.getHour()+1;
        List<String> horasList = new ArrayList<>();
        if (hora < 8) {
            hora = 8;
        }
        if (hora <= 19) {
            if (fecha.getMinute() > 0 && fecha.getMinute() < 30) {
                horasList.add(hora + ":30");
                hora++;
            }
            for (int i = hora; i <= 19; i++) {
                horasList.add(i + ":00");
                if (i != 19) {
                    horasList.add(i + ":30");
                }
            }
        }
        List<String> horasAll = new ArrayList<>();
        for (int i = 8; i <= 19; i++) {
            horasAll.add(i + ":00");
            if (i != 19) {
                horasAll.add(i + ":30");
            }
        }
        modelo.addAttribute("horaList", horasList);
        modelo.addAttribute("horasAll", horasAll);
        modelo.addAttribute("now", now);
        modelo.addAttribute("listaVentanilla", ventanillaService.listarVentanillas());
        modelo.addAttribute("listaServicios", servicioService.listar());
        modelo.addAttribute("username", authentication.getName());
        return "citas/formCita";
    }

    @GetMapping("/listar")
    @Secured("ROLE_EMPLEADO")
    public String listar(Model model, RedirectAttributes redirectAttributes) {
        List<Cita> listaCitas = citaService.listar();
        model.addAttribute("listaCitasConsulta", listaCitas);
        model.addAttribute("banderaListar", false);
        return "citas/listarCitas";
    }

    @GetMapping("/listar/personal")
    @Secured("ROLE_USER")
    public String listarPersonal(Model model, RedirectAttributes redirectAttributes,
            Authentication authentication) {
        User user = userServiceImpl.findUserByUsername(authentication.getName());
        List<Cita> listaCitas = citaService.findByUser(user);
        model.addAttribute("listaCitasPersonal", listaCitas);
        model.addAttribute("banderaListarPersonal", true);
        return "citas/listarCitasPersonal";
    }

    @GetMapping("/mostrar/{id}")
    @Secured("ROLE_USER")
    public String mostrar(@PathVariable long id, Model modelo, RedirectAttributes redirectAttributes,
            Authentication authentication) {

        Cita cita = citaService.mostrar(id);
        Transaccion transaccion = transaccionServiceImpl.findByCita(cita);
        boolean transaccionExist = false;
        if (transaccion != null) {
            transaccionExist = true;
        }
        modelo.addAttribute("transaccion", transaccionExist);
        if (cita != null) {
            if (cita.getUser().getUsername().equals(authentication.getName())) {
                modelo.addAttribute("cita", cita);
                List<DocumentoCita> listaDocs = documentoCitaService.findByidCita(cita);
                if (listaDocs.isEmpty()) {
                    modelo.addAttribute("bandera", false);
                } else {
                    modelo.addAttribute("bandera", true);
                    modelo.addAttribute("listaDocs", listaDocs);
                }
                return "citas/mostrarCita";
            }
            redirectAttributes.addFlashAttribute("msg_errorMostrar", "La cita no se encontró en su registro personal");
            return "redirect:/citas/listar/personal";
        }

        redirectAttributes.addFlashAttribute("msg_errorMostrar", "consulta fallida");
        return "redirect:/citas/listar";
    }

    @GetMapping("/editar/{id}")
    @Secured({ "ROLE_USER", "ROLE_EMPLEADO" })
    public String editar(@PathVariable long id, Model modelo, RedirectAttributes redirectAttributes) {
        Cita cita = citaService.mostrar(id);
        if (cita != null) {
            LocalDate now = LocalDate.now();
            LocalDateTime fechaEdit = LocalDateTime.now();
            int horaEdit = fechaEdit.getHour()+1;
            List<String> horasListEdit = new ArrayList<>();
            if (horaEdit < 8) {
                horaEdit = 8;
            }
            if (horaEdit <= 19) {
                if (fechaEdit.getMinute() > 0 && fechaEdit.getMinute() < 30) {
                    horasListEdit.add(horaEdit + ":30");
                    horaEdit++;
                }
                for (int i = horaEdit; i <= 19; i++) {
                    horasListEdit.add(i + ":00");
                    if (i != 19) {
                        horasListEdit.add(i + ":30");
                    }
                }
            }
            List<String> horasAllEdit = new ArrayList<>();
            for (int i = 8; i <= 19; i++) {
                horasAllEdit.add(i + ":00");
                if (i != 19) {
                    horasAllEdit.add(i + ":30");
                }
            }
            modelo.addAttribute("horaListEdit", horasListEdit);
            modelo.addAttribute("horasAllEdit", horasAllEdit);
            modelo.addAttribute("cita", cita);
            modelo.addAttribute("now", now);
            modelo.addAttribute("estadoUpdate", cita.isEstado());
            modelo.addAttribute("listaVentanilla", ventanillaService.listarVentanillas());
            modelo.addAttribute("listaServicios", servicioService.listar());
            return "citas/editarCita";
        }
        redirectAttributes.addFlashAttribute("msg_errorEditar", "Registro no encontrado");
        return "redirect:/citas/listar";
    }

    @PostMapping("/guardar")
    @Secured({ "ROLE_USER", "ROLE_EMPLEADO" })
    public String guardar(@ModelAttribute("cita") Cita cita, @RequestParam("hora") String hora, Model modelo,
            Authentication authentication,
            RedirectAttributes redirectAttributes) {
        boolean respuesta = false;
        String successMessage = "";
        String errorMessage = "";
        boolean bandera = true;
        if (cita.getIdCita() == null) { // Create
            List<Cita> listaDia = citaService.listarPorDiaAndVentanilla(cita.getFecha(),
                    cita.getVentanilla().getIdventanilla());
            for (int i = 0; i < listaDia.size(); i++) {
                if (hora.equals(listaDia.get(i).getHora())) {
                    errorMessage = "No se puede registrar una cita a la ventanilla "
                            + listaDia.get(i).getVentanilla().getNumero() + " por que ya cuenta con una";
                    bandera = false;
                }
            }
            if (bandera) {
                cita.setHora(hora);
                successMessage = "Cita Registrada Exitosamente";
            }
        } else { // Update
            List<Cita> listaDia = citaService.listarPorDiaAndVentanillaAndCita(cita.getFecha(),
                    cita.getVentanilla().getIdventanilla(), cita.getIdCita());
            for (int i = 0; i < listaDia.size(); i++) {
                if (hora.equals(listaDia.get(i).getHora())) {
                    errorMessage = "No se puede modificar una cita a la ventanilla "
                            + listaDia.get(i).getVentanilla().getNumero() + " por que ya cuenta con una";
                    bandera = false;
                }
            }
            if (bandera) {
                cita.setFecha(cita.getFecha());
                cita.setHora(cita.getHora());
                cita.setEstado(cita.isEstado());
                cita.setVentanilla(cita.getVentanilla());
                successMessage = "Cita Modificada Exitosamente";
            }
        }
        String retornoCitasExitoso = "";
        String retornoCitasFallido = "";
        User usuariologueadoCitas = userServiceImpl.findUserByUsername(authentication.getName());
        for (Rol rol : usuariologueadoCitas.getRols()) {
            if (rol.getName().equals("ROLE_USER")) {
                retornoCitasExitoso = "redirect:/citas/listar/personal";
                retornoCitasFallido = "redirect:/citas/crear";
            } else {
                retornoCitasExitoso = "redirect:/citas/listar";
                retornoCitasFallido = "redirect:/citas/editar/" + cita.getIdCita();
            }
        }
        if (bandera) {
            respuesta = citaService.guardar(cita);
            if (!respuesta) {
                errorMessage = "Registro Fallido por favor intente de nuevo";
            }
        }
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", successMessage);
            return retornoCitasExitoso;
        } else {
            redirectAttributes.addFlashAttribute("msg_errorRegistro", errorMessage);
            if (cita.getIdCita() == null) {
                return retornoCitasFallido;
            }
            return retornoCitasFallido;
        }
    }

    @GetMapping("/eliminar/{id}")
    @Secured({ "ROLE_USER", "ROLE_EMPLEADO" })
    public String eliminar(@PathVariable long id, RedirectAttributes redirectAttributes,
            Authentication authentication) {
        List<DocumentoCita> docs = documentoCitaService.findByidCita(citaService.mostrar(id));

        if (!docs.isEmpty()) {
            for (DocumentoCita documento : docs) {
                documentoCitaService.eliminar(documento.getIdDocumentoCita());
            }
        }
        Transaccion transaccion = transaccionServiceImpl.findByCita(citaService.mostrar(id));
        if (transaccion != null) {
            transaccionServiceImpl.eliminar(transaccion.getIdTransaccion());
        }
        boolean respuesta = citaService.eliminar(id);
        User user = userServiceImpl.findUserByUsername(authentication.getName());
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", "Eliminacion exitosa");
            for (Rol rol : user.getRols()) {
                if (rol.getName().equals("ROLE_USER")) {
                    return "redirect:/citas/listar/personal";
                }
            }
            return "redirect:/citas/listar";
        } else {
            redirectAttributes.addFlashAttribute("msg_errorEliminar", "Eliminación fallida");
            for (Rol rol : user.getRols()) {
                if (rol.getName().equals("ROLE_USER")) {
                    return "redirect:/citas/listar/personal";
                }
            }
            return "redirect:/citas/listar";
        }
    }
}