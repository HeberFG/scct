package mx.edu.utez.scct.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.scct.entity.Rol;
import mx.edu.utez.scct.entity.User;
import mx.edu.utez.scct.impl.UserServiceImpl;

@Controller
@RequestMapping()
public class HomeController {

    @Autowired
    UserServiceImpl userServiceImpl;

    @GetMapping("/")
    @Secured({ "ROLE_ADMIN", "ROLE_EMPLEADO", "ROLE_USER" })
    public String home(Model model, Authentication authentication) {
        if (authentication == null) {
            return "login";
        }
        String username = authentication.getName();
        User user = userServiceImpl.findUserByUsername(username);
        for (Rol rol : user.getRols()) {
            if (rol.getName().equals("ROLE_USER")) {
                return "redirect:/citas/listar/personal";
            }
        }
        model.addAttribute("roleList", user.getRols());
        return "index";
    }
}
