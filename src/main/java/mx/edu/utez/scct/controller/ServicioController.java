package mx.edu.utez.scct.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.scct.entity.Servicio;
import mx.edu.utez.scct.impl.DocumentoServiceImpl;
import mx.edu.utez.scct.impl.ServicioServiceImpl;

@Controller
@RequestMapping(value = "/servicios")
public class ServicioController {

    @Autowired
    ServicioServiceImpl servicioServiceImpl;

    @Autowired
    DocumentoServiceImpl documentoServiceImpl;

    @GetMapping("/listar")
    @Secured("ROLE_ADMIN")
    public String listar(Model model) {
        model.addAttribute("listaServicios", servicioServiceImpl.listar());
        return "servicios/listarServicios";
    }

    @GetMapping("/crear")
    @Secured("ROLE_ADMIN")
    public String crear(Servicio servicio, Model model) {
        model.addAttribute("listaDocumentos", documentoServiceImpl.listar());
        return "servicios/formServicio";
    }

    @PostMapping("/guardar")
    @Secured("ROLE_ADMIN")
    public String guardar(@Valid @ModelAttribute("servicio") Servicio servicio, BindingResult result,
            Model model, RedirectAttributes redirectAttributes) {
        boolean respuesta = false;
        String successMessage;
        if(servicio.getIdServicio() == null){
            respuesta = servicioServiceImpl.guardar(servicio);
            successMessage = "Servicio Registrado Exitosamente";
        }else{
            respuesta = servicioServiceImpl.guardar(servicio);
            successMessage = "Servicio Modificado Exitosamente";
        }
        if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                System.out.println("Error : " + error.getDefaultMessage());
            }
            return "servicios/formServicio";
        }
        if (respuesta) {
            redirectAttributes.addFlashAttribute("msg_success", successMessage);
            return "redirect:/servicios/listar";
        } else {
            redirectAttributes.addFlashAttribute("msg_error", "¡Registro de Servicio fallido!");
            return "redirect:/servicios/crear";
        }
    }

    @GetMapping("/eliminar/{id}")
    @Secured("ROLE_ADMIN")
    public String eliminar(@PathVariable long id, RedirectAttributes redirectAttributes) {
        Servicio servicio = servicioServiceImpl.mostrar(id);
        if (servicio != null) {
            servicio.setEstado(!servicio.getEstado());
            servicioServiceImpl.guardar(servicio);
            redirectAttributes.addFlashAttribute("msg_success", "¡Cambio exitoso!");
            return "redirect:/servicios/listar";
        } else {
            redirectAttributes.addFlashAttribute("msg_errorEliminar", "¡Cambio fallido!");
            return "redirect:/servicios/listar";
        }
    }

    @PostMapping("/mostrar")
    @Secured("ROLE_ADMIN")
    public String mostrar(@RequestParam("idServicio") long id, Model modelo, RedirectAttributes redirectAttributes) {
        Servicio servicio = servicioServiceImpl.mostrar(id);
        if (servicio != null) {
            modelo.addAttribute("servicio", servicio);
            return "servicios/mostrarServicio";
        }
        redirectAttributes.addFlashAttribute("msg_errorMostrar", "consulta fallida");
        return "redirect:/servicios/listar";
    }

    @PostMapping("/editar")
    @Secured("ROLE_ADMIN")
    public String editar(@RequestParam("idServicio") long id, Model modelo, RedirectAttributes redirectAttributes) {
        Servicio servicio = servicioServiceImpl.mostrar(id);
        if (servicio != null) {
            modelo.addAttribute("listaDocumentos", documentoServiceImpl.listar());
            modelo.addAttribute("servicio", servicio);
            return "servicios/editServicio";
        }
        redirectAttributes.addFlashAttribute("msg_error", "Registro no encontrado");
        return "redirect:/Servicios/listar";
    }
}
