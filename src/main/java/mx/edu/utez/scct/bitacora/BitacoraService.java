package mx.edu.utez.scct.bitacora;

import java.util.List;

public interface BitacoraService {

    List<Bitacora> listBitacora();

    Bitacora mostrar(Long id);
}
