package mx.edu.utez.scct.bitacora;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BitacoraServiceImpl implements BitacoraService {

    @Autowired
    BitacoraRepository bitacoraRepository;

    @Override
    public List<Bitacora> listBitacora() {
        return bitacoraRepository.findAll();
    }

    @Override
    public Bitacora mostrar(Long id) {
        Optional<Bitacora> bitacora = bitacoraRepository.findById(id);
        if (bitacora.isPresent()) {
            return bitacora.get();
        }
        return null;
    }

}
