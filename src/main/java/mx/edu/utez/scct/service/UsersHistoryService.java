package mx.edu.utez.scct.service;

import java.util.List;

import mx.edu.utez.scct.entity.UsersHistory;


public interface UsersHistoryService {
	
	List<UsersHistory> listAllUsers();

	UsersHistory findUserByUsername(String username);

}
