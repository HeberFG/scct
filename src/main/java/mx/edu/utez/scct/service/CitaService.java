package mx.edu.utez.scct.service;

import java.util.Date;
import java.util.List;

import mx.edu.utez.scct.entity.Cita;
import mx.edu.utez.scct.entity.User;

public interface CitaService {
    List<Cita> listar();

    boolean guardar(Cita cita);

    boolean eliminar(Long id);

    Cita mostrar(Long id);

    List<Cita> listarPorDiaAndVentanilla(Date dia, Long idVentanilla);

    List<Cita> listarPorDiaAndVentanillaAndCita(Date dia, Long idVentanilla, Long idCita);

    List<Cita> findByUser(User user);

    List<Cita> deleteByUser(User user);
}
