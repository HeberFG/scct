package mx.edu.utez.scct.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "username", length = 50)
    @Size(min = 3, max = 40, message = "El nombre debe contener de 3 a 40 caracteres")
    @NotBlank(message = "Este campo es obligatorio")
    @Pattern(regexp = "[a-z0-9]+@utez.edu.mx", message = "El correo introducido no cumple con el formato institucional")
    private String username;

    @Column(name = "nombre")
    @NotBlank(message = "El nombre no puede estar vacio")
    @Pattern(regexp = "[a-zA-Z áéíóúÁÉÍÓÚÑñ\\s]*", message = "Este campo solo debe contener letras")
    private String nombre;

    @Column(name = "apellido1")
    @NotBlank(message = "Debe de ingresar un Apellido Paterno")
    @Pattern(regexp = "[a-zA-Z áéíóúÁÉÍÓÚÑñ\\s]*", message = "Este campo solo debe contener letras")
    private String apellido1;

    @Column(name = "apellido2")
    @NotBlank(message = "Debe de ingresar un Apellido Materno")
    @Pattern(regexp = "[a-zA-Z áéíóúÁÉÍÓÚÑñ\\s]*", message = "Este campo solo debe contener letras")
    private String apellido2;

    @Column(name = "password", length = 250)
    @NotBlank(message = "La contraseñaes obligatoria")
    private String password;

    @Column(name = "enabled")
    @NotNull(message = "Este campo es obligatorio")
    private Boolean enabled;

    @ManyToMany
    @JoinTable(name = "authorities", joinColumns = @JoinColumn(name = "username"), inverseJoinColumns = @JoinColumn(name = "authority"))
    @NotNull(message = "Los roles son necesarios")
    private Set<Rol> rols = new HashSet<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Rol> getRols() {
        return rols;
    }

    public void setRols(Set<Rol> rols) {
        this.rols = rols;
    }

    @Override
    public String toString() {
        return "User [apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", enabled=" + enabled + ", nombre="
                + nombre + ", password=" + password + ", rols=" + rols + ", username=" + username + "]";
    }


    
}
