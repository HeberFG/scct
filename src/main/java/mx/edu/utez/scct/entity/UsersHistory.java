package mx.edu.utez.scct.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "users_history")
public class UsersHistory {

	  	@Id
	  	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private String id;
	  
	    @Column(name = "username", length = 50)
	    private String username;

	    @Column(name = "nombre")
	    private String nombre;

	    @Column(name = "apellido1")
	    private String apellido1;

	    @Column(name = "apellido2")
	    private String apellido2;


	    @Column(name = "enabled")
	    private Boolean enabled;
	    
	    @Column(name = "fecha")
	    private Date fecha;
	    

	    @Column(name = "hora")
	    private Date hora;

	    @Column(name = "accion", length = 50)
	    private String accion;

		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getUsername() {
			return username;
		}


		public void setUsername(String username) {
			this.username = username;
		}


		public String getNombre() {
			return nombre;
		}


		public void setNombre(String nombre) {
			this.nombre = nombre;
		}


		public String getApellido1() {
			return apellido1;
		}


		public void setApellido1(String apellido1) {
			this.apellido1 = apellido1;
		}


		public String getApellido2() {
			return apellido2;
		}


		public void setApellido2(String apellido2) {
			this.apellido2 = apellido2;
		}


		public Boolean getEnabled() {
			return enabled;
		}


		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}


		public Date getFecha() {
			return fecha;
		}


		public void setFecha(Date fecha) {
			this.fecha = fecha;
		}


		public Date getHora() {
			return hora;
		}


		public void setHora(Date hora) {
			this.hora = hora;
		}


		public UsersHistory(String id, String username, String nombre, String apellido1, String apellido2,
				Boolean enabled, Date fecha, Date hora) {
			super();
			this.id = id;
			this.username = username;
			this.nombre = nombre;
			this.apellido1 = apellido1;
			this.apellido2 = apellido2;
			this.enabled = enabled;
			this.fecha = fecha;
			this.hora = hora;
		}


		public UsersHistory() {
		}


		@Override
		public String toString() {
			return "UsersHistory [id=" + id + ", username=" + username + ", nombre=" + nombre + ", apellido1="
					+ apellido1 + ", apellido2=" + apellido2 + ", enabled=" + enabled + ", fecha=" + fecha + ", hora="
					+ hora + "]";
		}


		public String getAccion() {
			return accion;
		}


		public void setAccion(String accion) {
			this.accion = accion;
		}
	    
		
		
	    
	    
	    
	    
}
