package mx.edu.utez.scct.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "detalle_solicitante")
public class Solicitante {

    @Id
    @Column(name = "matricula")
    @Size(min = 10, max = 10, message = "La matricula debe contar solo con 10 caracteres")
    private String matricula;

    @Column(name = "telefono")
    @Size(min = 9, max = 11, message = "El telefono debe contener de 9 a 11 digitos")
    @Pattern(regexp = "[0-9]*", message = "Este campo solo debe contener numeros")
    private String telefono;

    @ManyToOne
    @JoinColumn(name = "carrera", nullable = false)
    @NotNull(message = "Debe seleccionar una carrera")
    private Carrera carrera;

    @ManyToOne
    @JoinColumn(name = "user", nullable = false)
    private User user;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Solicitante [carrera=" + carrera + ", matricula=" + matricula + ", telefono=" + telefono + ", user="
                + user + "]";
    }


    

}
