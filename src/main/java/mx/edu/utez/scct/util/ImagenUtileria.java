package mx.edu.utez.scct.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class ImagenUtileria {

	public static String guardarDocumento(MultipartFile multipartFile, String ruta) {

		String nombreArchivo = multipartFile.getOriginalFilename();
		System.err.println(nombreArchivo);

		try {
			System.err.println(ruta);
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        	String data = df.format(new Date());
        	String nuevoNombreArchivo = data.replaceAll(" ", "").replaceAll("-", "_").replaceAll(":", "_") + ".pdf";
			String rutaArchivo = ruta + "/" + nuevoNombreArchivo ;
			System.err.println(rutaArchivo);
			File archivo = new File(rutaArchivo);
			multipartFile.transferTo(archivo);
			return nuevoNombreArchivo;

		} catch (IOException e) {
			System.err.println(e.getMessage());
			return "null";
		}
	}
}
