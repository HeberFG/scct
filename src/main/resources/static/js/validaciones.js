function comparaHoras() {
  const inicio = document.getElementById("horaInicial");
  const final = document.getElementById("horaUltima");
  const vInicio = inicio.value;
  const vFinal = final.value;
  const tIni = new Date();
  const pInicio = vInicio.split(":");
  tIni.setHours(pInicio[0], pInicio[1]);
  const tFin = new Date();
  const pFin = vFinal.split(":");
  tFin.setHours(pFin[0], pFin[1]);
  var fechaValidacion = $("#fechaDia").val();
  var horaIn = $("#horaInicial").val();
  var horaFi = $("#horaUltima").val();
  var repeticiones = $("#repeticiones").val();
  var usuarios = $("#selectUsuarios").val();
  var ventanillas = $("#selectVentanilla").val();
  var fechaDia = new Date($("#fechaDia").val() + " 00:00:00");
  var today = new Date();
  today.setHours(0, 0, 0, 0);
  var horaInSplit = horaIn.split(":");
  var horaFinSplit = horaFi.split(":");
  var startHour = 8;
  var endHour = 19;
  if (fechaValidacion === "") {
    new Toast({
      message: "Por favor de agregar un dia para el horario",
      type: "danger",
    });
  } else if (fechaDia.getTime() < today.getTime()) {
    new Toast({
      message: "Por favor poner una fecha actual o futura",
      type: "danger",
    });
  } else if (repeticiones === "") {
    new Toast({
      message: "Por favor ingrese un número de repeticiones",
      type: "danger",
    });
  } else if (repeticiones < 0) {
    new Toast({
      message: "Solo se admiten numeros arriba de 0",
      type: "danger",
    });
  } else if (usuarios === "") {
    new Toast({
      message: "Por favor de escoger el usuario de atención para el horario",
      type: "danger",
    });
  } else if (ventanillas === "") {
    new Toast({
      message: "Por favor de escoger la ventanilla de atención para el horario",
      type: "danger",
    });
  } else if (tFin.getTime() < tIni.getTime()) {
    new Toast({
      message: "La hora final no debe ser Menor a la hora inicial",
      type: "danger",
    });
  } else if (tFin.getTime() === tIni.getTime()) {
    new Toast({
      message: "No puedes insertar las misma hora para la inicial y final",
      type: "danger",
    });
  } else if (horaIn === "") {
    new Toast({
      message:
        "Por favor de escoger la hora Inicial de atención para el horario",
      type: "danger",
    });
  } else if (horaFi === "") {
    new Toast({
      message: "Por favor de escoger la hora Final de atención para el horario",
      type: "danger",
    });
  } else if (
    horaInSplit != "" &&
    (horaInSplit[0] < startHour || horaInSplit[0] > endHour)
  ) {
    new Toast({
      message:
        "La hora inicial solo puede ser despues de las 8 de la mañana y antes de las 7 noche",
      type: "danger",
    });
  } else if (
    horaFinSplit != "" &&
    (horaFinSplit[0] < startHour || horaFinSplit[0] > endHour)
  ) {
    new Toast({
      message:
        "La hora final solo puede ser despues de las 8 de la mañana y antes de las 7 de la noche",
      type: "danger",
    });
  } else {
    var btn = document.getElementById("formRegistro");
    document.body.append(btn);
    btn.submit();
  }
}
function citas() {
  var diaValidacion = $("#fecha").val();
  var estado = $("#estadoInput").val();
  var selectVentanilla = $("#selectVentanilla").val();
  var selectServicio = $("#selectServicio").val();
  var horas = $("#horasInput").val();
  var dia = new Date($("#fecha").val() + " 00:00:00");
  var today = new Date();
  today.setHours(0, 0, 0, 0);
  if (diaValidacion === "") {
    new Toast({
      message: "Por favor de agregar un día para la cita",
      type: "danger",
    });
  } else if (selectVentanilla === "") {
    new Toast({
      message: "Por favor de elegir una ventanilla para la cita",
      type: "danger",
    });
  } else if (horas === "") {
    new Toast({
      message: "Por favor de elegir una hora para la cita",
      type: "danger",
    });
  } else if (selectServicio == "") {
    new Toast({
      message: "Por favor elegir un servicio para la cita",
      type: "danger",
    });
  } else if (dia.getTime() < today.getTime()) {
    new Toast({
      message: "Por favor poner una fecha actual o futura",
      type: "danger",
    });
  } else {
    var btn = document.getElementById("formRegistro");
    document.body.append(btn);
    btn.submit();
  }
}

function validaContraseña(password){
  var lowerCaseLetters = /[a-z]/g;
  var upperCaseLetters = /[A-Z]/g;
  var numbers = /[0-9]/g;

  var retorno = "";
  if(!password.match(numbers)){
    retorno = "La contraseña debe contener al menos un numero";
  }
  
  
  if(!password.match(lowerCaseLetters)){
    retorno = "La contraseña debe contener al menos una letra minuscula";
  }

  if(!password.match(upperCaseLetters)){
    retorno = "La contraseña debe contener al menos una letra mayuscula";
  }
  return retorno;
}

function validacionesSolicitante(){
  var matricula = $("#matricula").val();
  var telefono = $("#telefono").val();
  var selectCarreras = $("#selectCarreras").val();
  var email = $("#email1").val();
  var nombre = $("#nombre").val();
  var apellido1 = $("#apellido1").val();
  var apellido2 = $("#apellido2").val();
  var password = $("#password").val();
  var telefonoValidador = telefono.replace(/[^0-9]/g, '');
  var nombreValidador = nombre.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido1Validador = apellido1.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido2Validador = apellido2.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var passwordValidadorRaro = password.replace(/[^0-9a-zA-ZñÑ]/g, '');

  let ex = new RegExp(/[^a-z]+@utez.edu.mx/g);
  var emailValidador = email.includes("@utez.edu.mx")
  var contraValid = validaContraseña(password);
  

  if(matricula === "" && telefono === "" && selectCarreras === "" && email === "" && nombre === "" && apellido1 === "" && apellido2 === "" &&  password === "" ){
    new Toast({
      message: "Favor de llenar el formulario de registro",
      type: "danger",
    });
  }else if(!emailValidador){
    new Toast({
      message: "El correo introducido no cumple con el formato institucional",
      type: "danger",
    });
  }else if(matricula === "" ){
    new Toast({
      message: "Favor de ingresar su matricula",
      type: "danger",
    });
  }else  if(matricula.length < 10 || matricula.length > 10){
    new Toast({
      message: "La matricula debe contar solo con 10 caracteres",
      type: "danger",
    });
  }else if(telefono === "" ){
    new Toast({
      message: "Favor de ingresar su telefono",
      type: "danger",
    });
  }else if(telefono.length < 9 || telefono.length > 11  ){
    new Toast({
      message: "El telefono debe de contener de 9 a 11 digitos",
      type: "danger",
    });
  }else if(telefonoValidador != telefono ){
    new Toast({
      message: "El telefono solo debe contener números",
      type: "danger",
    });
  }else if(selectCarreras === "" ){
    new Toast({
      message: "Favor de escoger una carrera",
      type: "danger",
    });
  }else if(nombre === "" ){
    new Toast({
      message: "Favor de ingresar un nombre",
      type: "danger",
    });
  }else if(nombreValidador != nombre ){
    new Toast({
      message: "El nombre solo debe contener letras",
      type: "danger",
    });
  }else if(nombre.length < 2 || nombre.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido1 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido1Validador != apellido1 ){
    new Toast({
      message: "El apellido paterno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido1.length < 2 || apellido1.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido2 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido2Validador != apellido2 ){
    new Toast({
      message: "El apellido materno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido2.length < 2 || apellido2.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(password === ""){
    new Toast({
      message: "La contraseña no puede estar vacia",
      type: "danger",
    });
  }else if(password.length < 8 || password.length > 16){
    new Toast({
      message: "La contraseña debe tener un minimo de 8 y un maximo de 16 caracteres",
      type: "danger",
    });
  }else if(passwordValidadorRaro != password){
    new Toast({
      message: "La contraseña tiene caracteres no aceptables",
      type: "danger",
    });
  }else if(contraValid != ""){
    new Toast({
      message: contraValid,
      type: "danger",
    });
  }else {
    var btn = document.getElementById("formRegistroSolicitante");
    document.body.append(btn);
    btn.submit();
  }
}

function validacionesUser(){
  let estadoActivo = document.getElementById("estadoActivo").checked
  let estadoInactivo = document.getElementById("estadoInactivo").checked
  var email = $("#email1").val();
  var nombre = $("#nombre").val();
  var apellido1 = $("#apellido1").val();
  var apellido2 = $("#apellido2").val();
  var password = $("#password").val();
  let rolAdmin = document.getElementById("rolAdmin").checked
  let rolEmpleado = document.getElementById("rolEmpleado").checked

  var nombreValidador = nombre.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido1Validador = apellido1.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido2Validador = apellido2.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var passwordValidadorRaro = password.replace(/[^0-9a-zA-ZñÑ]/g, '');
  let ex = new RegExp(/[^a-z]+@utez.edu.mx/g);
  var emailValidador = email.includes("@utez.edu.mx")
  var contraValid = validaContraseña(password);
 


  if(email === "" && nombre === "" && apellido1 === "" && apellido2 === "" &&  password === "" && !rolAdmin && !rolEmpleado ){
    new Toast({
      message: "Favor de llenar el formulario de registro",
      type: "danger",
    });
  }else if(!rolAdmin && !rolEmpleado){
    new Toast({
      message: "Favor de elegir un rol para el usuario",
      type: "danger",
    });
  }else if(!emailValidador){
    new Toast({
      message: "El correo introducido no cumple con el formato institucional",
      type: "danger",
    });
  }else if(!estadoActivo  && !estadoInactivo){
    new Toast({
      message: "Favor de escoger el estado del usuario",
      type: "danger",
    });
  }else if(nombre === "" ){
    new Toast({
      message: "Favor de ingresar un nombre",
      type: "danger",
    });
  }else if(nombreValidador != nombre ){
    new Toast({
      message: "El nombre solo debe contener letras",
      type: "danger",
    });
  }else if(nombre.length < 2 || nombre.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido1 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido1Validador != apellido1 ){
    new Toast({
      message: "El apellido paterno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido1.length < 2 || apellido1.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido2 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido2Validador != apellido2 ){
    new Toast({
      message: "El apellido materno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido2.length < 2 || apellido2.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(password === ""){
    new Toast({
      message: "La contraseña no puede estar vacia",
      type: "danger",
    });
  }else if(password.length < 8 || password.length > 16){
    new Toast({
      message: "La contraseña debe tener un minimo de 8 y un maximo de 16 caracteres",
      type: "danger",
    });
  }else if(passwordValidadorRaro != password){
    new Toast({
      message: "La contraseña tiene caracteres no aceptables",
      type: "danger",
    });
  }else if(contraValid != ""){
    new Toast({
      message: contraValid,
      type: "danger",
    });
  }else {
    var btn = document.getElementById("formRegistroUser");
    document.body.append(btn);
    btn.submit();
  }
}



function validacionesUserEdit(){
 
  var email = $("#email1").val();
  var nombre = $("#nombre").val();
  var apellido1 = $("#apellido1").val();
  var apellido2 = $("#apellido2").val();
  var password = $("#password").val();
  let rolAdmin = document.getElementById("rolAdmin").checked
  let rolEmpleado = document.getElementById("rolEmpleado").checked


  var nombreValidador = nombre.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido1Validador = apellido1.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido2Validador = apellido2.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  let ex = new RegExp(/[^a-z]+@utez.edu.mx/g);
  var emailValidador = email.includes("@utez.edu.mx")

 

  if(email === "" && nombre === "" && apellido1 === "" && apellido2 === "" &&  password === "" && !rolAdmin && !rolEmpleado ){
    new Toast({
      message: "Favor de llenar el formulario de registro",
      type: "danger",
    });
  }else if(!rolAdmin && !rolEmpleado){
    new Toast({
      message: "Favor de escoger un rol para el usuario",
      type: "danger",
    });
  }else if(nombre === "" ){
    new Toast({
      message: "Favor de ingresar un nombre",
      type: "danger",
    });
  }else if(nombreValidador != nombre ){
    new Toast({
      message: "El nombre solo debe contener letras",
      type: "danger",
    });
  }else if(nombre.length < 2 || nombre.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(!emailValidador){
    new Toast({
      message: "El correo introducido no cumple con el formato institucional",
      type: "danger",
    });
  }else if(apellido1 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido1Validador != apellido1 ){
    new Toast({
      message: "El apellido paterno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido1.length < 2 || apellido1.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido2 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido2Validador != apellido2 ){
    new Toast({
      message: "El apellido materno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido2.length < 2 || apellido2.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(password === ""){
    new Toast({
      message: "La contraseña no puede estar vacia",
      type: "danger",
    });
  }else {
    var btn = document.getElementById("formEditUser");
    document.body.append(btn);
    btn.submit();
  }
}


function validacionesEditSolicitante(){
  var matricula = $("#matricula").val();
  var telefono = $("#telefono").val();
  var selectCarreras = $("#selectCarreras").val();
  var email = $("#email1").val();
  var nombre = $("#nombre").val();
  var apellido1 = $("#apellido1").val();
  var apellido2 = $("#apellido2").val();
  var password = $("#password").val();
  var telefonoValidador = telefono.replace(/[^0-9]/g, '');
  var nombreValidador = nombre.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido1Validador = apellido1.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  var apellido2Validador = apellido2.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  
  

  if(matricula === "" && telefono === "" && selectCarreras === "" && email === "" && nombre === "" && apellido1 === "" && apellido2 === "" &&  password === "" ){
    new Toast({
      message: "Favor de llenar el formulario de registro",
      type: "danger",
    });
  }else if(matricula === "" ){
    new Toast({
      message: "Favor de ingresar su matricula",
      type: "danger",
    });
  }else  if(matricula.length < 10 || matricula.length > 10){
    new Toast({
      message: "La matricula debe contar solo con 10 caracteres",
      type: "danger",
    });
  }else if(telefono === "" ){
    new Toast({
      message: "Favor de ingresar su telefono",
      type: "danger",
    });
  }else if(telefono.length < 9 || telefono.length > 11  ){
    new Toast({
      message: "El telefono debe de contener de 9 a 11 digitos",
      type: "danger",
    });
  }else if(telefonoValidador != telefono ){
    new Toast({
      message: "El telefono solo debe contener números",
      type: "danger",
    });
  }else if(selectCarreras === "" ){
    new Toast({
      message: "Favor de escoger una carrera",
      type: "danger",
    });
  }else if(nombre === "" ){
    new Toast({
      message: "Favor de ingresar un nombre",
      type: "danger",
    });
  }else if(nombreValidador != nombre ){
    new Toast({
      message: "El nombre solo debe contener letras",
      type: "danger",
    });
  }else if(nombre.length < 2 || nombre.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido1 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido1Validador != apellido1 ){
    new Toast({
      message: "El apellido paterno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido1.length < 2 || apellido1.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else if(apellido2 === "" ){
    new Toast({
      message: "Favor de ingresar el apellido paterno",
      type: "danger",
    });
  }else if(apellido2Validador != apellido2 ){
    new Toast({
      message: "El apellido materno solo debe contener letras",
      type: "danger",
    });
  }else if(apellido2.length < 2 || apellido2.length > 36 ){
    new Toast({
      message: "El nombre debe de tener minimo 2 y maximo 36 caracteres",
      type: "danger",
    });
  }else {
    var btn = document.getElementById("formRegistroSolicitante");
    document.body.append(btn);
    btn.submit();
  }
}

function validarDocumento(){
  var documento = $("#formFile").val();
  let estadoActivo = document.getElementById("estadoActivo").checked
  let estadoInactivo = document.getElementById("estadoInactivo").checked

  if(documento === ""){
    new Toast({
      message: "Favor de ingresar el documento requerido",
      type: "danger",
    })
  }else if(!estadoActivo && !estadoInactivo){
    new Toast({
      message: "Favor de escoger un estado del documento",
      type: "danger",
    })
  }else{
    var btn = document.getElementById("formDocumento");
    document.body.append(btn);
    btn.submit();
  }
}


function formDocumentoRegistro(){

  var nombre = $("#nombreDocumento").val();
  let estadoActivo = document.getElementById("estadoActivo").checked
  let estadoInactivo = document.getElementById("estadoInactivo").checked

  if(nombre === "" && !estadoActivo && !estadoInactivo){
    new Toast({
      message: "Favor de llenar los datos requeridos",
      type: "danger",
    })
  }else if(nombre ===""){
    new Toast({
      message: "Ingresa el nombre del documento",
      type: "danger",
    })
  }else if(nombre.length < 3 || nombre.length > 25){
    new Toast({
      message: "El nombre debe de tener como minimo 3 y maximo 25 caracteres",
      type: "danger",
    })
  }else if(!estadoActivo && !estadoInactivo){
    new Toast({
      message: "Favor de escoger un estado del documento",
      type: "danger",
    })
  }else{
    var btn = document.getElementById("formDocumentoAdmin");
    document.body.append(btn);
    btn.submit();
  }

}

function formDocumentoEdit(){

  var nombre = $("#nombreDocumento").val();
  var nombreValidador = nombre.replace(/[^a-zA-Z áéíóúÁÉÍÓÚÑñ]/g, '');
  if(nombre === ""){
    new Toast({
      message: "Ingresa el nombre del documento",
      type: "danger",
    })
  }else if(nombreValidador != nombre){
    new Toast({
      message: "El nombre del documento solo puede tener letras",
      type: "danger",
    })
  }else if(nombre.length < 3 || nombre.length > 25){
    new Toast({
      message: "El nombre debe de tener como minimo 3 y maximo 25 caracteres",
      type: "danger",
    })
  }else{
    var btn = document.getElementById("formDocumentoAdminEdit");
    document.body.append(btn);
    btn.submit();
  }

}


function validarServicioRegistro(){
  var nombreServicio = $("#nombreServicio").val();
  var precioServicio = $("#precioServicio").val();
  var descripcion = $("#descripcionServicio").val();
  var docuentosLista = $("#documentosLista").checked;
 console.log("nombre tamao:" + nombreServicio.length );
 console.log("desc tamao:" + descripcion.length );
  if(nombreServicio === "" && precioServicio === "" && descripcion ==="" && docuentosLista === undefined){
    new Toast({
      message: "Favor de ingresar los valores solicitados",
      type: "danger",
    })
  }else if(nombreServicio === ""){
    new Toast({
      message: "Ingresa el nombre del servicio",
      type: "danger",
    })
  }else if(nombreServicio.length < 10 || nombreServicio.length > 45 ){
    new Toast({
      message: "El nombre debe de tener un minimo 10 y un maximo de 45 caracteres",
      type: "danger",
    })
  }else if(descripcion === ""){
    new Toast({
      message: "Ingresar la descripción del servicio",
      type: "danger",
    })
  }else if(descripcion.length < 10 || descripcion.length > 45 ){
    new Toast({
      message: "La descripción debe tener un minimo de 10 caracteres y un maximo de 25",
      type: "danger",
    })
  }else if(precioServicio === ""){
    new Toast({
      message: "Ingresa el precio del servicio",
      type: "danger",
    })
  }else{
    var btn = document.getElementById("formServicio");
    document.body.append(btn);
    btn.submit();
  }

}

function validarServicioEdit(){
  var nombreServicio = $("#nombreServicio").val();
  var precioServicio = $("#precioServicio").val();
  var descripcion = $("#descripcionServicio").val();
  var docuentosLista = $("#documentosLista").checked;
 console.log("nombre tamao:" + nombreServicio.length );
 console.log("desc tamao:" + descripcion.length );
  if(nombreServicio === "" && precioServicio === "" && descripcion ==="" && docuentosLista === undefined){
    new Toast({
      message: "Favor de ingresar los valores solicitados",
      type: "danger",
    })
  }else if(nombreServicio === ""){
    new Toast({
      message: "Ingresa el nombre del servicio",
      type: "danger",
    })
  }else if(nombreServicio.length < 10 || nombreServicio.length > 45 ){
    new Toast({
      message: "El nombre debe de tener un minimo 10 y un maximo de 45 caracteres",
      type: "danger",
    })
  }else if(descripcion === ""){
    new Toast({
      message: "Ingresar la descripción del servicio",
      type: "danger",
    })
  }else if(descripcion.length < 10 || descripcion.length > 45 ){
    new Toast({
      message: "La descripción debe tener un minimo de 10 caracteres y un maximo de 25",
      type: "danger",
    })
  }else if(precioServicio === ""){
    new Toast({
      message: "Ingresa el precio del servicio",
      type: "danger",
    })
  }else{
    var btn = document.getElementById("formServicioEdit");
    document.body.append(btn);
    btn.submit();
  }

}

